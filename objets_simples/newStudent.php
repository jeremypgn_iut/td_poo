<?php

use peopleapp\personne\Etudiant;

session_start();

require_once "autoloader.php";
if($_POST){
    $student = new Etudiant($_POST["nom"]);
    $student->prenom = $_POST["prenom"];
    if($_POST["numero"] != ""){
        $student->num_etudiant = $_POST["numero"];
    }else{
        if(empty($_SESSION["students"])){
            $student->num_etudiant = 1;
        }else{
            $student->num_etudiant = unserialize(end($_SESSION["students"]))->num_etudiant+1;
        }
    }

    $_SESSION["students"][] = serialize($student);
    header('Location: /jeremypingeon/td_poo/objets_simples/newStudent.php');

}
if(isset($_GET["delete"])){
    foreach ($_SESSION["students"] as $key => $student){
        if(unserialize($student)->num_etudiant == $_GET["id"]){
            unset($_SESSION["students"][$key]);
        }
    }
    header('Location: /jeremypingeon/td_poo/objets_simples/newStudent.php');
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <title>Gestion des étudiants</title>
    <style>
        body{
            font-family: Arial;
        }
        .form-input{
            display: block;
        }
        .form-input label{
            display: block;
        }
        table, td, th {
            border: 1px solid black;
            padding: 5px;
        }
        table {
            border-collapse: collapse;
        }
        th {
            height: 30px;
        }
    </style>
</head>
<body>
    <h1>Gestion des étudiants</h1>
    <h2>Enregistrement d'un étudiant</h2>
    <form action="" method="post">
        <div class="form-input">
            <label for="numero">N° étudiant :</label>
            <input id="numero" type="number" name="numero" placeholder="Numéro de l'étudiant" value="<?php if(isset($_SESSION["students"]) && !empty(@$_SESSION["students"])){ echo unserialize(end($_SESSION["students"]))->num_etudiant+1; } ?>" />
        </div>
        <div class="form-input">
            <label for="prenom">Prénom :</label>
            <input id="prenom" type="text" name="prenom" placeholder="Prénom" required="required" />
        </div>
        <div class="form-input">
            <label for="nom">Nom :</label>
            <input id="nom" type="text" name="nom" placeholder="Nom" required="required" />
        </div>
        <input type="submit" value="Enregistrer"/>
    </form>
    <h2>Liste des étudiants</h2>
<?php if(isset($_SESSION["students"])){ ?>
    <table>
        <tr>
            <th>N°</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Action</th>
        </tr>
<?php foreach ($_SESSION["students"] as $student){ ?>
            <tr>
                <td><?= unserialize($student)->num_etudiant; ?></td>
                <td><?= unserialize($student)->prenom; ?></td>
                <td><?= unserialize($student)->nom; ?></td>
                <td><a href="?delete&id=<?= unserialize($student)->num_etudiant; ?>">Supprimer</a></td>
            </tr>
<?php } ?>
    </table>
<?php } ?>

</body>
</html>