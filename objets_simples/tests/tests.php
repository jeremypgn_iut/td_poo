<?php
echo "----------Tests unitaires----------\n\n";

if($dir = opendir('./'))
{
    while(($file = readdir($dir)))
    {
        if(substr(strrchr($file, '.'), 1) == "php"){
            if($file != "tests.php"){
                $output = exec("phpunit ".$file);
                echo "-----> ".$file."\n";
                echo $output."\n\n";
            }

        }
    }
}