<?php
require "../models/AfficheurPersonne.php";
require "../models/AfficheurEnseignant.php";

class AfficheurEnseignantTest extends \PHPUnit\Framework\TestCase {

    function testSubclass(){
        $this->assertTrue(is_subclass_of('AfficheurEnseignant',
        'AfficheurPersonne'), 
        "La classe AfficheurEnseignant doit hérité de AfficheurPersonne");
    }
    
    function testAfficherExists(){
        $mth = ['vueCourte', 'vueDetail' ];
        foreach($mth as $m){
            $this->assertTrue(method_exists('AfficheurEnseignant', $m), 
            "La classe AfficheurEnseignant doit concrétiser la méthode" .$m); 
        }
    }

    function testAfficherNotExists(){
        $rp = new ReflectionMethod('AfficheurPersonne', 'afficher');
        $this->assertEquals($rp->getDeclaringClass()->getname(),
        'AfficheurPersonne',  
        "La classe AfficheurEnseignant ne doit pas avoir de méthode afficher"); 
    }
    
}
