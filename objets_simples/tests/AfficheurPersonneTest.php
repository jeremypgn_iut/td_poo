<?php

require "../models/AfficheurPersonne.php";

class AfficheurPersonneTest extends \PHPUnit\Framework\TestCase {
    
    function testAbstract(){
        $rp = new ReflectionClass('AfficheurPersonne');
        $this->assertTrue($rp->isAbstract(),
        'La class AfficheurPersonne doit être abstraite');
    }


    function testAbstractMethods(){
        $mth = ['vueCourte', 'vueDetail' ];
        foreach($mth as $m){
            $rp = new ReflectionMethod('AfficheurPersonne', $m);
            $this->assertTrue($rp->isAbstract(),
            'La méthode '.$m.' doit être abstraite');
        }
    }
    
    function testAfficherExists(){
        $this->assertTrue(method_exists('AfficheurPersonne', 'afficher'), 
        "La classe AfficheurPersonne doit toujour avoir la  méthode afficher"); 
    }
    
}
