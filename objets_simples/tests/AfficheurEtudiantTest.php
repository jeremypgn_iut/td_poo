<?php
require "../models/AfficheurPersonne.php";
require "../models/AfficheurEtudiant.php";

class AfficheurEtudiantTest extends \PHPUnit\Framework\TestCase {

    function testSubclass(){
        $this->assertTrue(is_subclass_of('AfficheurEtudiant',
        'AfficheurPersonne'), 
        "La classe AfficheurEtudiant doit hérité de AfficheurPersonne");
    }
    
    function testAfficherExists(){
        $mth = ['vueCourte', 'vueDetail' ];
        foreach($mth as $m){
            $this->assertTrue(method_exists('AfficheurEtudiant', $m), 
            "La classe AfficheurEtudiant doit concrétiser la méthode" .$m); 
        }
    }

    function testAfficherNotExists(){
        $rp = new ReflectionMethod('AfficheurPersonne', 'afficher');
        $this->assertEquals($rp->getDeclaringClass()->getname(),
        'AfficheurPersonne',  
        "La classe AfficheurEtudiant ne doit pas avoir de méthode afficher"); 
    }
    
}
