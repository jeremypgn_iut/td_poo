<?php

require "../models/Personne.php";

class Personne2Test extends \PHPUnit\Framework\TestCase {
    
    function testAbstract(){
        $rp = new ReflectionClass('Personne');
        $this->assertTrue($rp->isAbstract(),
        'La class personne doit être abstraite');
    }
    
    function testConjointNotExists(){
        $this->assertClassNotHasAttribute('conjoint', 'Personne', 
        "La classe Personne ne doit plus avoir dr propriété conjoint");
    }
    
    function testPunitionNotExists(){
        $this->assertFalse(method_exists('Personne', 'ecrirePunition'), 
        "La classe Personne ne doit plus avoir de méthode ecrirePunition"); 

    }

 function testCompterExists(){
     $this->assertTrue(method_exists('Personne', 'compter'), 
     "La classe Personne doit toujour avoir la  méthode Compter"); 
     
    }
    
    
}
    
    
