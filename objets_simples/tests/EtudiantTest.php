<?php
require "../models/Personne.php";
require "../models/Etudiant.php";

class EtudiantTest extends \PHPUnit\Framework\TestCase {
    
    function testSubclass(){
        $this->assertTrue(is_subclass_of('Etudiant', 'Personne'), 
        "La classe Etudiant doit hérité de Personne");
    }
    
    function testProperties(){
        $this->assertClassHasAttribute( 'num_etudiant' , 'Etudiant');
        $this->assertClassHasAttribute( 'ref_formation', 'Etudiant');
        $this->assertClassHasAttribute( 'groupe'       , 'Etudiant');
    }
    
    function testVisibility(){
        $prop = [ 'num_etudiant' ,  'ref_formation', 'groupe'];
        
        foreach ( $prop as $p ){
            $rp = new ReflectionProperty('Etudiant', $p);
            $this->assertTrue($rp->isProtected(),
            'L\'attribut '.$p.' doit être protégé');
        }
    }
    
    function testMagic(){
        $this->assertTrue(method_exists('Etudiant', '__set'), 
        "La classe Etudiant n'a pas de méthode __set");

        $this->assertTrue(method_exists('Etudiant', '__get'), 
        "La classe Etudiant n'a pas de méthode __get");
    }

    function testEcrirePunitionExists(){
        $this->assertTrue(method_exists('Etudiant', 'ecrirePunition'), 
        "La classe Etudiant doit avoir une de méthode ecrirePunition");
    }
    
 
    
    private function createEtudiant(){
        $p = new Etudiant('Jagger');
        $p->prenom='Mick';
        $p->age=23;
        $p->adresse='5 ave of the Rock';
        $p->ville='Dartford';
        $p->codepostal=90210;
        $p->mail='mick.jagger@rolingstones.com';
        $p->mobile='+41 6 12 34 56 78';
        $p->idskype='jagsir';

        $p->num_etudiant='987654';
        $p->ref_formation='DUT Guitare';
        $p->groupe='C';

        return $p;
    }
 
    function testEtudiant(){
        $p1 = new Etudiant('Jagger');
        $this->assertEquals($p1->nom, 'Jagger');
    }
 
    function testPropertiesValues(){
        $p1 = $this->createEtudiant();
    
        $this->assertEquals($p1->prenom, 'Mick');
        $this->assertEquals($p1->age, 23);
        $this->assertEquals($p1->adresse, '5 ave of the Rock');
        $this->assertEquals($p1->ville, 'Dartford');
        $this->assertEquals($p1->codepostal, 90210);
        $this->assertEquals($p1->mail, 'mick.jagger@rolingstones.com');
        $this->assertEquals($p1->mobile, '+41 6 12 34 56 78');
        $this->assertEquals($p1->idskype, 'jagsir');
    
        $this->assertEquals($p1->num_etudiant, '987654');
        $this->assertEquals($p1->ref_formation, 'DUT Guitare');
        $this->assertEquals($p1->groupe, 'C');
    }

    function testConjointNotExists(){
        $this->assertClassNotHasAttribute('conjoint', 'Etudiant', 
        "La propriété conjoint ne dois pas exister dans la classe Etudiant");
    }


    function testCompter(){
        $p = new Etudiant('toto');
        $p-> age = 5;
    
        $this->assertThat($p->compter(5),
        $this->logicalOr($this->equalTo('0 1 2 3 4 5')  ,
        $this->equalTo('0 1 2 3 4 5 ') , 
        $this->equalTo("0\n1\n2\n3\n4\n5\n") ,
        $this->equalTo("0\n1\n2\n3\n4\n5"),
        $this->equalTo('0 1 2 3 4 5\n')  ,
        $this->equalTo('0 1 2 3 4 5 \n')  
        ));
    
    }

    function testEcrirePunition(){
        $p = new Etudiant('toto');
  
        $this->assertTrue(method_exists('Etudiant', 'EcrirePunition'), 
        "La classe Etudiant n'a pas de méthode EcrirePunition");
		        
        $this->assertThat($p->ecrirePunition("foo", 3),
        $this->logicalOr($this->equalTo("foo foo foo"),
        $this->equalTo("foo foo foo\n"), 
        $this->equalTo("foo\nfoo\nfoo\n"),
        $this->equalTo("foo\nfoo\nfoo")
        ));
    
    }

}
