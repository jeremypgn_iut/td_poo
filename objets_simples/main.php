<?php
/*
 * Jérémy PINGEON - CIASIE 2019
 */
session_start();
use peopleapp\afficheur\AfficheurEtudiant;
use peopleapp\personne\Etudiant;

require_once "autoloader.php";

$etudiant1 = new Etudiant("Pingeon");
$etudiant1->prenom = "Jérémy";
$etudiant1->ville = "Laxou";
$etudiant1->num_etudiant = "5481495954";
$etudiant1->ref_formation = "CIASIE";
$etudiant1->groupe = 1;

if(!isset($_COOKIE["etudiant1"])){
    setcookie("etudiant1", serialize($etudiant1));
}else{
    $afficheurEtudiant1 = new AfficheurEtudiant(unserialize($_SESSION["etudiant1"]));
    echo $afficheurEtudiant1->vueCourte();
}

if(!isset($_SESSION["etudiant1"])){
    $_SESSION["etudiant1"] = serialize($etudiant1);
}else{
    $afficheurEtudiant1 = new AfficheurEtudiant(unserialize($_SESSION["etudiant1"]));
    echo $afficheurEtudiant1->vueCourte();
}

/*
 * use peopleapp\personne\Etudiant;
use peopleapp\personne\Etudiant as Etu;
use peopleapp\afficheur as aff;
use peopleapp\personne\Enseignant;
$pingeon = new Etu("Pingeon");
$pingeon->prenom = "Jérémy";
$pingeon->ville = "Laxou";
$pingeon->num_etudiant = "5481495954";
$pingeon->ref_formation = "CIASIE";
$pingeon->groupe = 1;
$afficheurPingeon = new aff\AfficheurEtudiant($pingeon);
echo $afficheurPingeon->vueDetail();

echo $pingeon->ecrirePunition("Bonjour !", 5);
$pingeon->ajouterNotes('math', '12;14;8.5;10.25');
$pingeon->ajouterNotes('francais', '5;3;20');
var_dump($pingeon->notes);
try{
    echo $pingeon->calculerMoyenneMat("math");
}catch (\Exception $e){
    echo $e->getMessage()." (étudiant: ".$pingeon->prenom." ".$pingeon->nom.")";
}

var_dump($pingeon->calculerMoyenneGenerale());

$bob = new Etudiant("Guetta");
$bob->prenom = "Bob";
$bob->ajouterNotes('math', '1');

$grp = new peopleapp\personne\Groupe('Groupe 1', 'S 1', 'Cisiie');
$grp->ajouterEtudiant($pingeon);
$grp->ajouterEtudiant($bob);

var_dump($grp->calculerMoyenneGroupe("notes"));
echo "Moyenne groupe pour les maths: ".$grp->calculerMoyenneGroupeMat("math");

 */