<?php
namespace peopleapp\afficheur;

class AfficheurEnseignant extends AfficheurPersonne{
    public function vueCourte(){
        return "
            <div>".$this->personne->nom." ".$this->personne->prenom." habite à ".$this->personne->ville." , et enseigne dans la discipline ".$this->personne->discipline."</div>
        ";
    }

    public function vueDetail(){
        return "
            <div>
                <h1>".$this->personne->nom." ".$this->personne->prenom." (enseignant)</h1>
                <ul>
                    <li>Adresse : ".$this->personne->adresse." ".$this->personne->codepostal." ".$this->personne->ville."</li>
                    <li>Age : ".$this->personne->age."</li>
                    <li>Mail : ".$this->personne->mail."</li>
                    <li>ID Skype : ".$this->personne->idskype."</li>
                    <li>Mobile : ".$this->personne->mobile."</li>
                    <li>Discipline : ".$this->personne->discipline."</li>
                    <li>Composante : ".$this->personne->composante."</li>
                    <li>Bureau : ".$this->personne->bureau."</li>
                </ul>
            </div>
        ";
    }
}