<?php
namespace peopleapp\afficheur;

abstract class AfficheurPersonne{
    public $personne;

    public function __construct($personne)
    {
        $this->personne = $personne;
    }

    abstract protected function vueCourte();

    abstract protected function vueDetail();

    public function afficher($selecteur){
        switch ($selecteur){
            case "court":
                return $this->vueCourte();
                break;
            case "long":
                return $this->vueDetail();
                break;
            default:
                //do nothing
        }
    }
}