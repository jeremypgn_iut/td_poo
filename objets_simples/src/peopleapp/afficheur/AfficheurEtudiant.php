<?php
namespace peopleapp\afficheur;

class AfficheurEtudiant extends AfficheurPersonne{
    public function vueCourte(){
        return "
            <div>".$this->personne->prenom." ".$this->personne->nom." habite à ".$this->personne->ville.", il est étudiant dans le groupe ".$this->personne->groupe."</div>
        ";
    }

    public function vueDetail(){
        return "
            <div>
                <h1>".$this->personne->nom." ".$this->personne->prenom." (étudiant)</h1>
                <ul>
                    <li>Adresse : ".$this->personne->adresse." ".$this->personne->codepostal." ".$this->personne->ville."</li>
                    <li>Age : ".$this->personne->age."</li>
                    <li>Mail : ".$this->personne->mail."</li>
                    <li>ID Skype : ".$this->personne->idskype."</li>
                    <li>Mobile : ".$this->personne->mobile."</li>
                    <li>N° étudiant : ".$this->personne->num_etudiant."</li>
                    <li>Formation : ".$this->personne->ref_formation."</li>
                    <li>Groupe : ".$this->personne->groupe."</li>
                </ul>
            </div>
        ";
    }
}