<?php
namespace peopleapp\personne;

class Enseignant extends Personne{
    protected $discipline, $composante, $bureau;
    protected $conjoint;

    public function ajouterConjoint(Personne $personne){
        $this->conjoint = $personne;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}