<?php
namespace peopleapp\personne;



class Etudiant extends Personne {
    protected $num_etudiant, $ref_formation ,$groupe;
    protected $notes = [];

    public function ecrirePunition($phrase, $nb_repetition){
        $string = "";
        for($i=1; $i<=$nb_repetition;$i++){
            $string .= $phrase."\n";
        }
        return $string;
    }

    public function ajouterNote($matiere, $valeur){
        $this->notes[$matiere][] = $valeur;
    }

    public function ajouterNotes($matiere, $notes){
        $notesArray = explode(";", $notes);

        foreach ($notesArray as $note){
            $this->ajouterNote($matiere, $note);
        }
    }

    public function calculerMoyenneMat($matiere){

            if(!array_key_exists($matiere, $this->notes)){
                throw new \Exception("La matière n'existe pas.");
                exit();
            }

            $total = 0;

            foreach ($this->notes[$matiere] as $note){
                $total += $note;
            }
            return $total/$this->nbNotes($matiere);
    }

    public function calculerMoyenneGenerale(){
        $moyennes = [];


        foreach ($this->notes as $matiere => $notesMatiere){
            $totalMatiere = 0;
            foreach ($notesMatiere as $note){
                $totalMatiere+= $note;
            }

            $moyennes[$matiere] = $totalMatiere/$this->nbNotes($matiere);
        }

        $total= 0;
        foreach ($this->notes as $matiere => $notesMatiere){
            foreach ($notesMatiere as $note){
                $total+= $note;
            }
        }
        $moyennes["generale"] = $total/$this->nbNotes();

        return $moyennes;
    }

    public function nbNotes($matiere = ""){
        if($matiere != ""){
            $total = 0;
            foreach ($this->notes[$matiere] as $note){
                $total++;
            }
            return $total;
        }else{
            $total = 0;
            foreach ($this->notes as $matiere){
                foreach ($matiere as $note){
                    $total++;
                }
            }
            return $total;
        }
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}