<?php
namespace peopleapp\personne;

class Groupe {
    public $groupe, $semestre, $formation;
    public $liste = [];

    public function __construct($groupe, $semestre, $formation)
    {
        $this->groupe = $groupe;
        $this->semestre = $semestre;
        $this->formation = $formation;
    }

    public function ajouterEtudiant(Etudiant $etu){
        $this->liste[] = $etu;
    }

    public function calculerMoyenneGroupeMat($matiere){
        $totalGroupe = 0;
        foreach ($this->liste as $etudiant){
            try{
                $moyenneEtuMatiere = $etudiant->calculerMoyenneMat($matiere);
            }catch (\Exception $e){
                throw new \Exception("L'étudiant ".$etudiant->prenom." ".$etudiant->nom." n'a pas de note dans cette matière");
                exit();
            }
            $totalGroupe += $moyenneEtuMatiere;
        }

        $moyenneGroupe = $totalGroupe/count($this->liste);
        return $moyenneGroupe;
    }

    public function calculerMoyenneGroupe($type = ""){
        $moyennes = [];

        foreach ($this->liste as $etudiant){
            $moyennes[$etudiant->nom] = round($etudiant->calculerMoyenneGenerale()["generale"],2);
        }
        switch ($type){
            case "noms":
                ksort($moyennes);
                break;
            case "notes":
                arsort($moyennes);
                break;
            default:
                //do nothing
        }
        return $moyennes;
    }
}