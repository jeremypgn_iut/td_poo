<?php
namespace peopleapp\personne;

abstract class Personne{
    protected $nom, $prenom, $age, $adresse, $ville, $codepostal, $mail, $mobile, $idskype;

    public function __construct($nom)
    {
        $this->nom = $nom;
    }

    public function __toString(){
        return json_encode([
            "nom" => $this->nom,
            "prenom" => $this->prenom,
            "age" => $this->age,
            "adresse" => $this->adresse,
            "ville" => $this->ville,
            "codepostal" => $this->codepostal,
            "mail" => $this->mail,
            "mobile" => $this->mobile,
            "idskype" => $this->idskype
        ]);
    }


    public function compter(){
        $string = "";
        for($i=0;$i<=$this->age;$i++){
            $string .= $i." ";
        }
        return $string;
    }

    public function __get($name)
    {
        /*if(!property_exists(Personne::class, $name)){
            throw new \Exception('L\'attribut n\'existe pas.');
        }*/
        return $this->$name;
    }

    public function __set($name, $value)
    {
        if(!property_exists(Personne::class, $name)){
            throw new \Exception('L\'attribut n\'existe pas.');
        }
        $this->$name = $value;
    }
}