<?php
require_once "autoloader.php";
$db_config = parse_ini_file("db.ini");
$db = new \PDO( 'mysql:host='.$db_config["host"].';dbname='.$db_config["dbname"], $db_config["username"], $db_config["password"], [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);

$request = new \core\Http\HttpRequest();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
</head>
<body>
<?php
if(isset($request->get)){
    foreach ($request->get as $key => $value){
        echo "nom du paramètre : ".$key."<br />";
        echo "valeur du paramètre : ".$value;
    }
}

if(isset($request->post["text"])){
    $text = htmlentities($request->post["text"]);

    $res = $db->prepare("SELECT count(id) AS nb FROM groupe WHERE code_grpe = ?");
    $res->execute([$text]);
    if($res->fetch()["nb"] == 0){
        echo "Le groupe \"" . $text . "\" n'existe pas";
    }else {


        $res = $db->prepare("SELECT etudiant.* FROM etudiant JOIN groupe ON groupe.id = etudiant.id_gpe WHERE code_grpe = ?");
        $res->execute([$text]);
        $etudiants = $res->fetchAll();
        if ($etudiants) {
            ?>
            <a href="" style="border: black solid 1px; padding: 5px; color: black; text-decoration: none;">Retour</a>
            <table style="margin-top: 10px;">
                <tr>
                    <th>#</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                </tr>
                <?php foreach ($etudiants as $etudiant) { ?>
                    <tr>
                        <td><?= $etudiant["id"]; ?></td>
                        <td><?= $etudiant["prenom"]; ?></td>
                        <td><?= strtoupper($etudiant["nom"]); ?></td>
                    </tr>
                <?php } ?>
            </table>

            <?php
        } else {
            echo "Aucun étudiant ne se trouve dans le groupe \"" . $text . "\"";
        }
    }
}else{
    echo core\HtmlFormat\Form::generateBasic();
}
?>
</body>
</html>
